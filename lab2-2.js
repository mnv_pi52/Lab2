/*1*/
function isIPAddress(ip){
    var regExp = /^(?:(?:25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d\d?)$/;

    var result = ip.match(regExp);

    if(!result){
        return false;
    }else{
        return true;
    }
}

/*2*/
function findRGBA(text){
    var regExp = /rgba\([ ]*([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])[ ]*,[ ]*([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])[ ]*,[ ]*([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])[ ]*,[ ]*((0\.(\d)*|1([.]0+)?))[ ]*\)/g;
    var result = (text.match(regExp))[0];
    return result;
}

/*3*/
function findHexColor(text){
    var regExp = /#(([a-fA-F0-9]{6})|([a-fA-F0-9]{3}))\b/g;

    var result = (text.match(regExp));
    return result;
}

/*4*/
function findTags(text, tag){
    var regExp = new RegExp('<\/{0,1}'+tag+'>','g');

    var result = text.match(regExp);
    return result;
}
/*5*/
function findPosNum(text){
    var regExp = /[^-]\b\d+(\.\d+)?\b/g;

    var result = (text.match(regExp));
    return result;
}

/*6*/
function findDates(text){
    var regExp = /(([0-9]{4})-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01]))\b/g;

    var result = (text.match(regExp));
    return result;
}